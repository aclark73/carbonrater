from django.contrib import admin
from mileage import models

class VehicleTypeAdmin(admin.ModelAdmin):
    list_display = ('year', 'make', 'model', 'variant')
admin.site.register(models.VehicleType, VehicleTypeAdmin)

class VehicleAdmin(admin.ModelAdmin):
    list_display = ('owner', 'name')
admin.site.register(models.Vehicle, VehicleAdmin)

class MileageRecordAdmin(admin.ModelAdmin):
    list_display = ('vehicle', 'date', 'mileage', 'gallons', 'filled')
admin.site.register(models.MileageRecord, MileageRecordAdmin)
