from django import forms

class CalendarInput(forms.DateInput):
    input_type='date'
    def __init__(self, attrs=None):
        default_attrs = {'class': 'calendar'}
        if attrs:
            default_attrs.update(attrs)
        super(CalendarInput, self).__init__(default_attrs)
    
class NumericInput(forms.TextInput):
    input_type='number'
    def __init__(self, attrs=None):
        default_attrs = {'class': 'numeric'}
        if attrs:
            default_attrs.update(attrs)
        super(NumericInput, self).__init__(default_attrs)
    