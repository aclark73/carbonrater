from datetime import date
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.aggregates import Max
from django.db.models.signals import post_save, post_delete
from django.dispatch.dispatcher import receiver
from django.utils.translation import ugettext_lazy as _
from south.modelsinspector import add_introspection_rules

class YearField(models.IntegerField):
    description = _("Integer")
    def get_internal_type(self):
        return "YearField"

    def formfield(self, **kwargs):
        defaults = {'min_value': 1990, 'max_value': date.today().year+5}
        defaults.update(kwargs)
        return super(YearField, self).formfield(**defaults)

add_introspection_rules([], ["^carbonrater\.mileage\.models\.YearField"])

class VehicleType(models.Model):
    year          = YearField()
    make          = models.CharField(max_length=100)
    model         = models.CharField(max_length=100)
    variant       = models.CharField(max_length=100)
    
    def name(self):
        return "{self.year} {self.make} {self.model} {self.variant}".format(self=self)
    
    def __str__(self):
        return self.name()

class Vehicle(models.Model):
    vehicle_type  = models.ForeignKey(VehicleType, blank=True, null=True, on_delete=models.SET_NULL)
    name          = models.CharField(max_length=100)
    owner         = models.ForeignKey(User, blank=False, null=False, editable=False)
    stated_mpg    = models.FloatField()
    actual_mpg    = models.FloatField(blank=True, null=True)
    mileage       = models.FloatField(default=0)
    
    @models.permalink
    def get_absolute_url(self):
        return ('mileage.views.vehicle', [str(self.id)])
    
    def __str__(self):
        return self.name

MILEAGE_ORDER_ERROR="Mileages and dates are out of order. You recorded {mileage.mileage} miles on {mileage.date}."
class MileageRecord(models.Model):
    vehicle       = models.ForeignKey(Vehicle, editable=False,
                    help_text='Vehicle')
    date          = models.DateField(
                    help_text='Date recorded')
    mileage       = models.FloatField(
                    help_text='Current mileage')
    gallons       = models.FloatField(blank=True, null=True,
                    help_text='Number of gallons filled')
    filled        = models.BooleanField(default=True,
                    help_text='True if tank was topped off')

    mpg           = models.FloatField(editable=False, blank=True, null=True,
                    help_text='Calculated MPG')

    def clean(self):
        prev_query = self.vehicle.mileagerecord_set.filter(date__lte=self.date).order_by('-date')
        if self.pk:
            prev_query = prev_query.exclude(pk=self.pk)
        if prev_query:
            prev_mileage = prev_query[0]
            if prev_mileage is not None and prev_mileage.mileage >= self.mileage:
                raise ValidationError(MILEAGE_ORDER_ERROR.format(mileage=prev_mileage))
    
        next_query = self.vehicle.mileagerecord_set.filter(date__gte=self.date).order_by('date')
        if self.pk:
            next_query = next_query.exclude(pk=self.pk)
        if next_query:
            next_mileage = next_query[0]
            if next_mileage is not None and next_mileage.mileage <= self.mileage:
                raise ValidationError(MILEAGE_ORDER_ERROR.format(mileage=next_mileage))

    class Meta:
        ordering = ['date']


