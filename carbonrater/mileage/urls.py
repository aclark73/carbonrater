from django.conf.urls.defaults import patterns, include, url
from django.conf import settings

urlpatterns = patterns('',
    
    url(r'^$', 'mileage.views.index', name='home'),

    url(r'^{0}$'.format(settings.LOGIN_PAGE), 'mileage.views.login', name='login',
        kwargs={'template_name':'login.html'}),
    url(r'^{0}$'.format(settings.LOGOUT_PAGE), 'django.contrib.auth.views.logout', name='logout',
        kwargs={'next_page':settings.BASE_URL}),

    url(r'^{0}$'.format(settings.ACCOUNT_PAGE), 
        'mileage.views.account', name='account'),
    
    url(r'^vehicle/(\d+)$', 'mileage.views.vehicle', name='vehicle'),
    url(r'^vehicle/(\d+)/edit$', 'mileage.views.vehicle_edit', name='vehicle_edit'),
    url(r'^vehicle/new$', 'mileage.views.vehicle_edit', name='vehicle_new'),
    
    url(r'^vehicle/(\d+)/mileage/(\d+)$', 'mileage.views.mileage_edit', name='mileage_edit'),
    url(r'^vehicle/(\d+)/mileage/new$', 'mileage.views.mileage_edit', name='mileage_new'),
    url(r'^vehicle/(\d+)/mileage/(\d+)/delete$', 'mileage.views.mileage_delete', name='mileage_delete'),
    
)
