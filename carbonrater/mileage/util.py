from django.core.exceptions import ValidationError

class MileageSet(object):

    def __init__(self):
        self.end_mileage = None
        self.clear()

    def clear(self):
        self.mileages = []
        self.start_mileage = self.end_mileage
        self.end_mileage = None
        self.mpg_miles = None
        self.mpg_gallons = 0
        self.is_complete = False
        self.mpg = None

    def add_mileage(self, mileage):
        if self.is_complete:
            raise Exception("Mileage added to complete set")
        self.mileages.append(mileage)
        self.end_mileage = mileage.mileage
        if mileage.gallons is None:
            self.mpg_gallons = None
        elif self.mpg_gallons is not None:
            self.mpg_gallons += mileage.gallons
        if mileage.filled:
            self.is_complete = True
            if self.mpg_gallons is not None and self.start_mileage is not None:
                self.mpg_miles = self.end_mileage - self.start_mileage
                self.mpg = round(self.mpg_miles / self.mpg_gallons, 1)
    
    def save(self):
        for mileage in self.mileages:
            if mileage.mpg != self.mpg:
                mileage.mpg = self.mpg
                mileage.save()
        self.clear()


def update_mileages(vehicle):
    
    mileage_records = vehicle.mileagerecord_set.order_by('date').all()

    max_mileage = 0
    mpg_miles = 0
    mpg_gallons = 0
    mileage_set = MileageSet()
    for mileage in mileage_records:
        max_mileage = mileage.mileage
        mileage_set.add_mileage(mileage)
        if mileage_set.is_complete:
            if mileage_set.mpg_gallons is not None and mileage_set.mpg_miles is not None:
                mpg_miles += mileage_set.mpg_miles
                mpg_gallons += mileage_set.mpg_gallons
            mileage_set.save()
    mileage_set.save()
    mpg = None
    if mpg_miles > 0 and mpg_gallons > 0:
        mpg = round(mpg_miles / mpg_gallons, 1)
    if vehicle.mileage != max_mileage or vehicle.actual_mpg != mpg:
        vehicle.mileage = max_mileage
        vehicle.actual_mpg = mpg
        vehicle.save()
