(function() {
  var $, _ref;

  $ = jQuery;

  if ((_ref = window.CarbonRater) == null) {
    window.CarbonRater = {};
  }

  CarbonRater.Page = (function() {

    function Page() {
      this.initInputs();
      null;
    }

    Page.prototype.initInputs = function() {
      this.initConfirmInputs();
      this.initNumericInputs();
      this.initDateInputs();
      return null;
    };

    Page.prototype.initConfirmInputs = function() {
      $('input.confirm').click(function(e) {
        if (!confirm("Are you sure?")) {
          return e.preventDefault();
        }
      });
      return null;
    };

    Page.prototype.initNumericInputs = function() {
      if (window.IS_MOBILE != null) {
        $("input.numeric").each(function(i, elem) {
          var $elem, decimal, fracInput, intInput;
          $elem = $(elem);
          $elem.hide();
          decimal = $elem.val().split('.');
          intInput = $('<input type="tel" size="7" pattern="[0-9]*"/>');
          intInput.val(decimal[0]);
          fracInput = $('<input type="tel" size="2" pattern="[0-9]*"/>');
          fracInput.val(decimal[1]);
          $elem.parent().append(intInput, ".", fracInput);
          return $elem.parent("form").submit(function() {
            $elem.val(parseFloat("" + (intInput.val()) + "." + (fracInput.val())));
            return null;
          });
        });
      }
      return null;
    };

    Page.prototype.initDateInputs = function() {};

    return Page;

  })();

  $(function() {
    return CarbonRater.page = new CarbonRater.Page();
  });

}).call(this);
