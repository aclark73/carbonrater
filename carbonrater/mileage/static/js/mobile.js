$(function() {
  $("input.numeric").each(function(i, elem) {
    $(elem).hide();
    var decimal = $(elem).val().split('.');
    var intInput = $('<input type="tel" size="7" pattern="[0-9]*"/>');
    intInput.val(decimal[0]);
    var fracInput = $('<input type="tel" size="2" pattern="[0-9]*"/>');
    fracInput.val(decimal[1]);
    $(elem).parent().append(intInput, ".", fracInput);
    $(elem).parent("form").submit(function() {
      $(elem).val(parseFloat(""+intInput.val()+"."+fracInput.val()));
    });
  });
});
