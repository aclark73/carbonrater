(function() {
  var $, _ref;

  $ = jQuery;

  if ((_ref = window.CarbonRater) == null) {
    window.CarbonRater = {};
  }

  CarbonRater.Page = (function() {

    function Page() {
      this.initInputs();
    }

    Page.prototype.initInputs = function() {
      this.initConfirmInputs();
      this.initNumericInputs();
      return this.initDateInputs();
    };

    Page.prototype.initConfirmInputs = function() {
      return $('input.confirm').click(function(e) {
        if (!confirm("Are you sure?")) {
          return e.preventDefault();
        }
      });
    };

    Page.prototype.initNumericInputs = function() {
      if (window.IS_MOBILE != null) {
        return $("input.numeric").each(function(i, elem) {
          var $elem, $fracInput, $intInput, decimal;
          $elem = $(elem);
          $elem.hide();
          decimal = $elem.val().split('.');
          $intInput = $('<input type="tel" size="7" class="input-small" style="text-align:right" pattern="[0-9]*" placeholder="0" />');
          $intInput.val(decimal[0]);
          $fracInput = $('<input type="tel" size="2" class="input-mini" pattern="[0-9]*" placeholder="0" />');
          $fracInput.val(decimal[1]);
          $elem.after($intInput, ".", $fracInput);
          return $elem.closest("form").submit(function() {
            return $elem.val(parseFloat("" + ($intInput.val()) + "." + ($fracInput.val())));
          });
        });
      }
    };

    Page.prototype.initDateInputs = function() {};

    return Page;

  })();

  $(function() {
    return CarbonRater.page = new CarbonRater.Page();
  });

}).call(this);
