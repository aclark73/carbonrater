require([
     // Require the basic 2d chart resource
    "dojox/charting/Chart",
     
    // Require the theme of our choosing
    "dojox/charting/themes/Claro",
 
    //  We want to plot Lines 
    "dojox/charting/plot2d/Lines",
     
    //  We want to use Markers
    "dojox/charting/plot2d/Markers",
 
    //  We'll use default x/y axes
    "dojox/charting/axis2d/Default",
 
    // Wait until the DOM is ready
    "dojo/domReady!"
], function(Chart, theme){

  // Define the data
  var chartData = [10000,9200,11811,12000,7662,13887,14200,12222,12000,10009,11288,12099];
   
  // Create the chart within it's "holding" node
  var chart = new Chart("chartNode");

  // Set the theme
  chart.setTheme(theme);

  // Add the only/default plot 
  chart.addPlot("default", {
      type: "Lines",
      markers: true
  });
   
  // Add axes
  chart.addAxis("x");
  chart.addAxis("y", { min: 5000, max: 15000, vertical: true, fixLower: "major", fixUpper: "major" });

  // Add the series of data
  chart.addSeries("SalesThisDecade",chartData);

  // Render the chart!
  chart.render();
   
});  
}
