function DataSet() {
  this.min = null;
  this.max = null;
  this.data = [];
  
  this.add = function(date, value) {
    this.data.push([date, value]);
    if (this.min == null || this.min > value) {
      this.min = value;
    }
    if (this.max == null || this.max < value) {
      this.max = value;
    }
  }
};

var chart;
$(function() {
  var miles_per_gallon = new DataSet();
  var miles_per_day = new DataSet();
  var gallons_per_day = new DataSet();
  
  var last_epoch = null;
  var last_mileage = null;
  for (var i in data) {
    var epoch = data[i][0];
    var date = new Date(epoch*1000);
    var mileage = data[i][1];
    var gallons = data[i][2];
    var mpg = data[i][3];
    
    if (mpg) {
      miles_per_gallon.add(date, mpg);
    }
    if (last_epoch && last_mileage) {
      var dt = (epoch - last_epoch) / (60*60*24);
      var dm = (mileage - last_mileage);
      miles_per_day.add(date, dm/dt);
      if (gallons) {
        gallons_per_day.add(date, gallons/dt);
      }
    }
    last_epoch = epoch;
    last_mileage = mileage;
  }

  
  chart = $.jqplot('chart', [miles_per_gallon.data, miles_per_day.data], {
    series:[{
      highlighter: {
        tooltipContentEditor: function(str, seriesIndex, pointIndex, plot) {
          var point = plot.series[seriesIndex].data[pointIndex];
          var dateStr = $.jsDate.strftime(point[0], "%#m/%#d/%y");
          return $.jqplot.sprintf("%s<br/>%.1f MPG", dateStr, point[1]);
        }
      }
    }, {
      highlighter: {
        formatString: "%s<br/>%.1f Miles/Day"
      },
      yaxis:'y2axis'
    }],
    seriesDefaults: {
      showMarker: !IS_MOBILE
    },
    legend: {
      show: true,
      labels: ['MPG', 'Miles/Day'],
      showSwatches: true
    },
    axesDefaults: {
      labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
      tickRenderer: $.jqplot.CanvasAxisTickRenderer,
      tickOptions: {
      }
    },
    axes: {
      xaxis: {
        renderer: $.jqplot.DateAxisRenderer,
        tickOptions: {
          formatString: '%#m/%y',
          angle: -45
        } 
      },
      yaxis: {
        min: 0,
        label: IS_MOBILE ? "" : "MPG",
        showTicks: !IS_MOBILE,
        tickOptions: {
          formatString: '%d'
        },
        autoscale: true
      },
      y2axis:{
        min: 0,
        label: IS_MOBILE ? "" : "Miles/Day",
        showTicks: !IS_MOBILE,
        labelOptions: {
          angle: 90
        },
        autoscale: true
      }
    },
    highlighter: {
      show: !IS_MOBILE,
      sizeAdjust: 7.5,
      useAxesFormatters: false
    },
    cursor: {
      show: false
    }
  });
});