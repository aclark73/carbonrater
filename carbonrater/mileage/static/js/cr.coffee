#= require jquery
$ = jQuery

window.CarbonRater ?= {}

class CarbonRater.Page
  constructor: ->
    @initInputs()
    
  initInputs: ->
    @initConfirmInputs()
    @initNumericInputs()
    @initDateInputs()
  
  initConfirmInputs: ->
    $('input.confirm').click( (e) ->
      if not confirm("Are you sure?")
        e.preventDefault()
    )
  
  initNumericInputs: ->
    if window.IS_MOBILE?
      $("input.numeric").each( (i, elem) ->
        $elem = $(elem)
        $elem.hide()
        decimal = $elem.val().split('.')
        $intInput = $('<input type="tel" size="7" class="input-small" style="text-align:right" pattern="[0-9]*" placeholder="0" />')
        $intInput.val(decimal[0])
        $fracInput = $('<input type="tel" size="2" class="input-mini" pattern="[0-9]*" placeholder="0" />')
        $fracInput.val(decimal[1])
        $elem.after($intInput, ".", $fracInput);
        $elem.closest("form").submit( ->
          $elem.val(parseFloat("#{$intInput.val()}.#{$fracInput.val()}"))
        )
      )
  
  initDateInputs: ->
      
$ ->
  CarbonRater.page = new CarbonRater.Page()
