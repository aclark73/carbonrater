from datetime import date
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.views import login as _login
from django.core.exceptions import ValidationError
from django.forms.fields import FloatField
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.widgets import DateInput, TextInput
from django.http import HttpResponseForbidden, Http404
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template.context import RequestContext
from mileage import util
from mileage.models import Vehicle, MileageRecord
from mileage.widgets import NumericInput, CalendarInput
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext as _
from django.contrib import auth, messages

def index(request):
    if request.user.is_authenticated():
        return redirect('account')
    else:
        return redirect('login')

# User Account

def account(request, user=None):
    if user is None:
        user = request.user
    return render_to_response('account.html', {
        'user': user,
    },
    context_instance=RequestContext(request))

class UserForm(forms.ModelForm):
    class Meta:
        model = User

class LoginForm(AuthenticationForm):
    username = forms.CharField(label=_("Username"), max_length=30,
        widget=TextInput(attrs={'autocapitalize': 'off'}))

def login(*args, **kwargs):
    return _login(authentication_form=LoginForm, *args, **kwargs)

def account_edit(request, user=None):
    if user is None:
        user = request.user
    elif user != request.user:
        if not request.user.is_administrator:
            raise HttpResponseForbidden
    if request.method == 'POST':
        form = UserForm(request.POST, instance=user) 
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = UserForm(instance=request.user)

    return render_to_response('account_edit.html', {
        'form': form,
    },
    context_instance=RequestContext(request))

# Vehicle

def vehicle(request, vehicle_id):
    vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    mileage_form = None  
    if vehicle.owner == request.user:
        mileage_record = MileageRecord(
            date = date.today(),
        )
        mileage_form = MileageRecordForm(instance=mileage_record)
    return render_to_response('vehicle.html', {
        'vehicle': vehicle,
        'mileage_form': mileage_form,
    }, context_instance=RequestContext(request))
    
class VehicleForm(forms.ModelForm):
    class Meta:
        model = Vehicle        

def vehicle_edit(request, vehicle_id=None):
    vehicle = None
    if vehicle_id is not None:
        vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
        if vehicle.owner_id != request.user.id:
            raise Http404()
    else:
        vehicle = Vehicle(owner=request.user)

    if request.method == 'POST':
        form = VehicleForm(request.POST, instance=vehicle) 
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = VehicleForm(instance=vehicle)

    return render_to_response('vehicle_edit.html', {
        'form': form,
        },
        context_instance=RequestContext(request))

# Mileage

class MileageRecordForm(forms.ModelForm):
    
    def add_error(self, error):
        self._errors[NON_FIELD_ERRORS] = self.error_class(error.messages)
        
    class Meta:
        model = MileageRecord
        widgets = {
            'date' : CalendarInput(),
            'mileage' : NumericInput(),
            'gallons' : NumericInput(),
        }

def get_mileage_record(request, vehicle_id, mileage_record_id=None):
    vehicle = get_object_or_404(Vehicle, pk=vehicle_id)
    if vehicle.owner_id != request.user.id:
        raise Http404("Not your vehicle")

    mileage_record = None
    if mileage_record_id is not None:
        mileage_record = get_object_or_404(MileageRecord, pk=mileage_record_id)
        if mileage_record.vehicle_id != vehicle.id:
            raise Http404("Vehicle/mileage mismatch")
    else:
        mileage_record = MileageRecord(vehicle=vehicle)
    return (vehicle, mileage_record)

def mileage_edit(request, vehicle_id, mileage_record_id=None):
    vehicle, mileage_record = get_mileage_record(request, vehicle_id, mileage_record_id)
    form = None
    if request.method == 'POST':
        # action is specified by name of submit button
        if 'cancel' in request.POST:
            return redirect(vehicle.get_absolute_url())
        elif 'delete' in request.POST:
            mileage_record.delete()
            util.update_mileages(vehicle)
            messages.info(request, "Deleted mileage record")
            return redirect(vehicle.get_absolute_url())
        else:
            form = MileageRecordForm(request.POST, instance=mileage_record) 
            if form.is_valid():
                mileage_record = form.save()
                util.update_mileages(vehicle)
                mileage_record = MileageRecord.objects.get(id=mileage_record.id)
                messages.info(request, "You got %.1f MPG" % mileage_record.mpg)
                return redirect(vehicle.get_absolute_url())
    else:
        form = MileageRecordForm(instance=mileage_record) 

    return render_to_response('mileage_edit.html', {
        'vehicle': vehicle,
        'form': form,
    },
    context_instance=RequestContext(request))

def mileage_delete(request, vehicle_id, mileage_record_id):
    vehicle, mileage_record = get_mileage_record(request, vehicle_id, mileage_record_id)
    mileage_record.delete()
    util.update_mileages(vehicle)
    return redirect(vehicle.get_absolute_url())
