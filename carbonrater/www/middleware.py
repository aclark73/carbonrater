import re

class UserAgent(object):
    def __init__(self, user_agent):
        self.raw = user_agent
        
        pattern = r'phone'
        prog = re.compile(pattern, re.IGNORECASE)
        match = prog.search(user_agent)

        self.mobile = True if match else False

class BrowserDetectionMiddleware(object):

    header = 'HTTP_USER_AGENT'
    mobile_pattern = re.compile(r'phone', re.IGNORECASE)

    def process_request(self, request):
        user_agent = ''
        if request.META.has_key(self.header):
            user_agent = request.META[self.header]
        request.is_mobile = bool(self.mobile_pattern.search(user_agent))  
