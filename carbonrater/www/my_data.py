from datetime import date

MY_DATA = '''
2005-08-22|0|0.0|
2008-04-09|22153|13.9|t
2008-05-19|22620|18.3|t
2008-06-06|22973|13.6|t
2008-06-30|23397|16.3|f
2008-07-31|23747|13.9|t
2008-08-15|24055|12.0|t
2008-08-24|24605|21.1|t
2008-09-17|24983|13.9|t
2008-10-11|25304|12.4|t
2008-10-21|25691|14.3|t
2008-11-11|26042|14.0|t
2009-01-03|26373|13.7|t
2009-02-04|26708|13.7|t
2009-02-27|27071|12.1|t
2009-03-14|27441||t
2009-04-10|27786|13.5|t
2009-05-02|28102|14.0|t
2009-05-25|28377|10.0|t
2009-06-25|28977||t
2009-07-14|29358|15.0|t
2009-08-04|29708|13.9|t
2009-09-14|30481|14.5|t
1276744744|34126 6
1277066611|34262 13
1279474076|34585 13.2
1280374501|34955 13.1
1282190518|35327 14.3
1282501073|35427 4.2
1283359177|36054 14.4
1287073609|36464 13.1
1288586933|36804 14
1291224594|37137 14.3
1292466507|37475 13.6
1295543950|37805 14.5
1300585442|38235 18.8
1302381162|38577 13.4
1304883683|38880 12.8
1306699906|39284 12.7
1309138965|39639 13.1
1312151153|39974 13.6
1313212701|40173 7.6
1313726444|40536 13
1317582960|41126 12.2
1320907590|41659 23.4
2012-01-14|41934|13.3|t
2012-01-14|42197|12.5|t
'''

my_data = []
for data in MY_DATA.splitlines():
    if data == '':
        continue
    try:
        parts = data.split('|')
        if len(parts) == 2:
            mileage_date = date.fromtimestamp(float(parts[0]))
            text = parts[1].split()
            if len(text) != 2:
                raise Exception("Couldn't read sms text {0}".format(parts[1]))
            mileage = text[0]
            gallons = text[1]
            full = 't'
        elif len(parts) == 4:
            dp = parts[0].split('-')
            mileage_date = date(int(dp[0]),int(dp[1]),int(dp[2]))
            mileage = parts[1]
            gallons = parts[2]
            full = 't' if parts[3] == 't' else 'f'
        else:
            raise Exception("Invalid line {0}".format(data))
        my_data.append((mileage_date.strftime('%Y-%m-%d'), mileage, gallons, full))
    except Exception as e:
        print("Failed to read line", e)

for data in my_data:
    print "\
insert into mileage_mileagerecord ('vehicle_id', 'date', 'mileage', 'gallons', 'filled') \
values (1, '{d[0]}', {d[1]}, {d[2]}, '{d[3]}');".format(d=data)
