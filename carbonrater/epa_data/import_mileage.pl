my @keys1 = ( "MFR", "CAR LINE", "CITY MPG (GUIDE)", "HWY MPG (GUIDE)", 
            "DISPLACEMENT", "NUMB CYL", "TRANS", "FUEL TYPE", "TURBO" );
my @keys2 = ( "Manufacturer", "carline name", "cty", "hwy", "displ", "cyl",
                "trans", "fl", "HACK_TURBO" );

my $keys = [
	["make", ["MFR", "Manufacturer"]],
	["model", ["CAR LINE", "carline name"]],
	["city_mpg", ["CITY MPG (GUIDE)", "cty"]],
	["hwy_mpg", ["HWY MPG (GUIDE)", "hwy"]],
	["disp", ["DISPLACEMENT", "displ"]],
	["cylinders", ["NUMB CYL", "cyl"]],
	["trans", ["TRANS", "trans"]],
	["turbo", ["TURBO", "t"]]
	];
	

my %keyMap;
foreach my $k (@$keys) {
	my ($key, $v) = @$key;
	foreach my $val (@$v) {
		$keyMap{$val} = $key;
	}
}

my @uncapsList = qw(KIA RIO CAB CAR EOS NEW VUE SKY);
my %uncaps;
foreach my $u (@uncapsList) {
	$uncaps{$u} = 1;
}


sub FixCase1 {
	my ($name) = @_;	
	if (length($name) > 3 || $uncaps{$name}) {
		$name =~ s/(\w+)/\u\L$1/;
	}
	return $name;
}

sub FixCase {
	my ($name) = @_;
	
	$name =~ s/[A-Z]{3,}/FixCase1($&)/ge;
	
	return $name;
}

sub CreateId {
	my ($d) = @_;	
	
	return "$d->{$mfrK} $d->{$modelK} " . ($d->{$turboK} ? "Turbo " : "") .  
            "($d->{$dispK}L $d->{$cylK}-cyl $d->{$transK})";
}

sub HandleQuote {
	my ($s) = @_;
	
	$s =~ s/,/:/g;
	return $s;
}

my $file = $ARGV[0];
my $year;
if ($file =~ /\d{4}/) {
	$year = $&;
} elsif ($file =~ /\d{2}/) {
    if ($& < 50) {
        $year = 2000 + $&;
    } else {
        $year = 1900 + $&;
    }
} else {
	die "Couldn't get year from $file\n";
}

my $key = <>;
my @keys = split(/,/, $key);
foreach $i (0..$#keys) {
    $keys[$i] =~ s/\"//g;
}

my @keyMap;
if ($keys[0] eq "CLASS") {
    @keyMap = @keys1;
} else {
    @keyMap = @keys2;
}

$mfrK = $keyMap[0];
$modelK = $keyMap[1];
$cityK = $keyMap[2];
$hwyK = $keyMap[3];
$dispK = $keyMap[4];
$cylK = $keyMap[5];
$transK = $keyMap[6];
$fuelK = $keyMap[7];
$turboK = $keyMap[8];

my %ids;

while (<>) {
	s/\"[^\"]*\"/HandleQuote($&)/ge;
	my (@data) = split(/,/, $_);
	if ($#data != $#keys) {
		print STDERR "Data count mismatch! $#data vs $#keys\n";
	}
	my %d;
	for my $i (0..$#data) {
		$data[$i] =~ s/\"//g;
		$d{$keys[$i]} = $data[$i];
	}
	$d{$mfrK} = &FixCase($d{$mfrK});
	$d{$modelK} = &FixCase($d{$modelK});
	$d{$transK} =~ s/\(.*\)//;

	my $name = &CreateId(\%d);

	if ($ids{$name}) {
		my $other = $ids{$name};
		my $thisFuel = $d{$fuelK};
		my $otherFuel = $other->{$fuelK};
					
		if ($thisFuel eq 'R' && $otherFuel eq 'P') {
			$other->{$turboK} = 1;
			$otherName = &CreateId($other);
			$ids{$otherName} = $other;
		}
		elsif ($otherFuel eq 'R' && $thisFuel eq 'P') {
			$d{$turboK} = 1;
			$name = &CreateId(\%d);
		}
		else {
			print STDERR "Duplicate! $name\n";
		}
	}
	$ids{$name} = \%d;
}

foreach my $k (sort keys(%ids)) {
	my $data = $ids{$k};
	my $variant = $data->{$dispK} . "L " . $data->{$cylK} . "-cyl " . ($data->{$turboK} ? "Turbo " : "") .  $data->{$transK};
	print qq(INSERT INTO auto_models (mfr, name, variant, year, city, highway) values ("$data->{$mfrK}", "$data->{$modelK}", "$variant", $year, $data->{$cityK}, $data->{$hwyK});\n);
}

